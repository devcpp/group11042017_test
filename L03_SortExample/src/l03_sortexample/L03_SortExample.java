package l03_sortexample;

public class L03_SortExample {

    public static void main(String[] args) {
        int[] arrData = {8, 3, 10, 5, 7, 1};
        
        boolean isContinue;
        int j = 0;
        int nCountInter = 0;
        do{
            isContinue = false;
            j++;
            for (int i=0; i<(arrData.length-j); i++){
                nCountInter++;
                if (arrData[i] > arrData[i+1]){
                    isContinue = true;
                    int nTmp = arrData[i];
                    arrData[i] = arrData[i+1];
                    arrData[i+1] = nTmp;
                }
            }
        } while(isContinue);
        
        for (int nVla:arrData){
            System.out.print(nVla + ", ");
        }
        
        System.out.println("");
        System.out.println("nCountInter = " + nCountInter);
        
    }
    
}
