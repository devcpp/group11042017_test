package l03_arrayexample;

import java.util.Scanner;

public class L03_ArrayExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Enter count: ");
        int nCount = scanner.nextInt();
        
        if (nCount<3){
            System.err.println("Count <3!!!!");
            return;
        }
        
        float[] arrData = new float[nCount];
        
        for (int i=0; i<arrData.length; i++){
            System.out.println("Enter [" + (i + 1) + "]: ");
            arrData[i] = scanner.nextFloat();
            
            if(arrData[i]<=0){
                System.out.println("[" + (i-- + 1) + "]:  <0!!!!");
            }
        }
        
        float fSum = 0;
        for(float fVal:arrData){
            fSum += fVal;
        }
        
        System.out.println("fSum = " + fSum);
    }
    
}
