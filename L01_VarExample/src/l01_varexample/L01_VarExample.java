package l01_varexample;

import java.util.Scanner;

public class L01_VarExample {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //System.out.println("Hello, WORLD!!!!!!");
        int countPoints = 1676;
        //float fVal = (float)1.5; - не правильно
        float fVal = 1.5f;
        
        int a = scan.nextInt();
        int b = scan.nextInt();
        
        int nSum = a+b;
        int nOstDel = a%b;
        float fDel = ((float)a)/b;
         
        
        System.out.println("nSum = " + nSum);
        System.out.println("nOstDel = " + nOstDel);
        System.out.println("fDel = " + fDel);
    }
    
}
