package l04_stringexample;

import java.util.Scanner;

public class L04_StringExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Enter num:");
        int i = scanner.nextInt();
        
        scanner.nextLine();
        System.out.println("Enter String:");
        String str = scanner.nextLine();
        
        System.out.println("i = " + i);
        System.out.println("str = " + str);
        
        String str2 = "             ";
        
        System.out.println("isEmpty -> " + str2.isEmpty());
        String str3 = str2.trim();
        System.out.println("isEmpty -> " + str2.isEmpty());
        System.out.println("isEmpty -> " + str3.isEmpty());
        
        String str4 = null;
        
        if (str4!=null && str4.isEmpty()){
            System.out.println("isEmpty");
        } else {
            System.out.println("null or !isEmpty");
        }
        
        if("".equals(str4)){
            
        }
    }
    
}
